# Como utilizar

1. Clone o repositório.
2. Abra com o vscode.
3. Abra o terminal do vs code e inicialize o container: `docker compose up` ou com o alias `make up`.
4. Em outra aba de terminal acesse o container: `docker exec -it phpexamples` sh ou com o alias `make php`.
5. Crie pastas de exercício tal como a pasta hello-world. Para ver pastas digite o comando `ls`.
6. Para executar o programa criado digite `cd <pasta-do-exercicio>`, por exemplo, `cd hello-world`. Em seguida, execute o programa. Por exemplo, `php hello.php`

Obs.: caso o programa leia entrada de dados utilize o pipe `<` e informe o arquivo com os dados. Por exemplo, `php hello.php <input1.txt`.